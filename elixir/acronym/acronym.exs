defmodule Acronym do
  @doc """
  Generate an acronym from a string.
  "This is a string" => "TIAS"
  """
  @spec abbreviate(string) :: String.t()
  def abbreviate(string) do
    string
    |> String.split(" ")
    # Upper case first letter of each word (titlecase would also lowercase the
    # rest, we want to leave those alone)
    |> Enum.map(fn(s) -> Regex.replace(~r/^\w{1}/, s, &String.upcase/1) end)
    # Remove non-upperase letters
    |> Enum.map(fn(s) -> String.replace(s, ~r/[^A-Z]/, "") end)
    |> Enum.join("")
    |> String.upcase
  end

  # Characters to use in acronym:
  # * First letter of each word
  # * capital letters
end
