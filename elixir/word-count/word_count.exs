defmodule Words do

  # Accept word chars, digits and hyphens in unicode
  @word_expression ~r/[^\w\d-]+/u

  @doc """
  Count the number of words in the sentence.

  Words are compared case-insensitively.
  """
  @spec count(String.t) :: map
  def count(sentence) do
    for {word, words} <- group_by_word(sentence), into: %{} do
      {word, length(words)}
    end
  end

  defp group_by_word(sentence) do
    sentence
    |> String.split([" ", "_"])
    |> Enum.map(fn(s) -> normalize_string(s) end)
    |> Enum.reject(fn(s) -> (! is_wordish?(s) || s == "" )end)
    |> Enum.group_by(&(&1))
  end

  defp normalize_string(s) do
    String.replace(s, @word_expression, "")
    |> String.downcase
  end

  defp is_wordish?(s) do
    ! String.match?(s, @word_expression)
  end

end
