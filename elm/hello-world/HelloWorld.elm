module HelloWorld (..) where

helloWorld : Maybe String -> String
helloWorld name =
  case name of
    Just s -> "Hello, " ++ s ++ "!"
    _ -> "Hello, World!"
